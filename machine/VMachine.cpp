#include "VMachine.h"
#include <fstream>
#include <iostream>
#include <lib/Tryte.h>
#include <hash_fun.h>
using namespace Poseidon;


VMachine::VMachine()
{

}


VMachine::~VMachine()
{

}


void VMachine::mov(const std::vector<std::string>& argv)
{
    if ( argv.size() != 2 )
        throw std::string("wrong arguments size");

    Tryte tryte = Tryte::fromString(argv[1]);
    std::hash<std::string> strHash;
    int hash = strHash(argv[0]);

    m_memory.insertTryte(hash, tryte);
}



void VMachine::sub(const std::vector<std::string>& argv)
{
    if ( argv.size() != 2 )
        throw std::string("wrong arguments size");

    std::hash<std::string> strHash;

    int firstHash = strHash(argv[0]);
    int secondHash = strHash(argv[1]);
    Tryte res = m_memory.findTryte(firstHash);
    res -= m_memory.findTryte(secondHash);

    m_memory.insertTryte(firstHash, res);
}


/*
 * execute without using Processor class
 *-----------------------------------------
 */
void VMachine::execute()
{
    std::ifstream fin("instructions");

    while ( fin.peek() != EOF ) {

        std::string chunk;
        std::getline(fin, chunk);

        int space = chunk.find(" ");
        std::string instr = chunk.substr(0, space);
        std::vector<std::string> argv;
        while ( space != -1 ) {
            int newSpace = chunk.find(" ", ++space);
            argv.push_back(chunk.substr(space, newSpace - space));
            space = newSpace;
        }

        // execute
        (this->*m_instrMap.at(instr))(argv);

        std::cout << m_memory.toString() << std::endl;
    }

    std::hash<std::string> hash;
    std::cout << m_memory.findTryte(hash(std::string("cx"))).toString() << std::endl;
}


/*
 * execute with Processor class
 *-----------------------------------------
 */
void VMachine::exec()
{
    std::ifstream fin("instructions");

    while ( fin.peek() != EOF ) {

        std::string chunk;
        std::getline(fin, chunk);

        int space = chunk.find(" ");
        std::string instr = chunk.substr(0, space);
        std::vector<std::string> argv;
        while ( space != -1 ) {
            int newSpace = chunk.find(" ", ++space);
            argv.push_back(chunk.substr(space, newSpace - space));
            space = newSpace;
        }


        if ( instr == "mov" )
            m_processor.move(m_regMap.at(argv[0]), Tryte::fromString(argv[1]));
        else if ( instr == "sum" )
            m_processor.summ(m_regMap.at(argv[0]), m_regMap.at(argv[1]));

        std::cout << m_processor.regToString(m_regMap.at(argv[0])) << std::endl;
    }
}

