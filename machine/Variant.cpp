#include "Variant.h"
using namespace Poseidon;


Variant::Variant()
{

}


Variant::Variant(const Tryte& tryte)
{
    m_data = new Tryte(tryte);
    m_currentType = Tryte_t;
}


Variant::~Variant()
{
    clear(m_currentType);
}


void Variant::clear(const Type& t)
{
    if ( t == Tryte_t )
        delete (Tryte*)m_data;
    else if ( t == Trit_t )
        delete (Trit*)m_data;
    else
        return;
}


Variant& Variant::operator=(const Variant& v)
{
    if ( this == &v )
        return *this;

    clear(m_currentType);
    if ( v.m_currentType == Tryte_t )
        m_data = new Tryte(v.getTryte());
    else // if v.currentType == Nothing
        ;

    m_currentType = v.m_currentType;
    return *this;
}




Tryte Variant::getTryte() const
{
    if ( m_currentType == Tryte_t )
        return *((Tryte*)m_data);
    else
        throw std::string("Variant stores other data type");
}
