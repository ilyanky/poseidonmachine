#ifndef TREE_H
#define TREE_H

#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <machine/Variant.h>

namespace Poseidon {


enum BalanceFactor {
    RightTreble,
    LeftTreble,

    LeftOverweight,
    RightOverweight,
    MidOverweight,

    Balanced
};


struct Node {
    Node*  left;
    Node*  right;
    Node*  mid;
    int    size;
    int    height;

    int     key;
    Variant value;


    Node() {
        left = nullptr;
        right = nullptr;
        mid = nullptr;
        key = 0;
        size = 1;
        height = 1;
    }

    Node(const int& _key, const Variant& _value) : Node() {
        key = _key;
        value = _value;
    }

    Node(const int& _key) {
        key = _key;
    }
};



class Tree
{

private:

    typedef Node* (Tree::*Rotation)(Node* const) const;


    Node* m_root;
    static const std::map<BalanceFactor, Rotation> m_rotationMap;


    Node*        insert(Node* const _node, Node* const _dataNode) const;
    void         _insert(Node*& _node, const int& _key);
    void        insert(const int& _key, const Variant& _value);


    void         _toString(const Node* const _node, std::string* const str) const;
    void         fixSize(Node* const node) const;
    int          height(const Node* const node) const;
    int          diff(const Node* const one, const Node* const two) const;
    const Node*  sizeDiff(const Node* const leftright, const Node* const mid,
                         const int& pSize) const;
    int          size(const Node* const node) const;
    void         fixHeight(Node* const node) const;
    void         fixMeta(Node* const node) const;


    Node*        balance(Node* const node) const;


    Node*        rotLeft(Node* const node) const;
    Node*        rotRight(Node* const node) const;
    Node*        leftTreble(Node* const node) const;
    Node*        rightTreble(Node* const node) const;
    Node*        rotMid(Node* const node) const;


    Node* insertions (Node* _node, Node* const _dataNode) const;

    void clear(Node* const node) const;
    Node* find(Node* const _node, const Node* const dataNode) const;

public:

    Tree();
    ~Tree();


    void insertTryte(const int& key, const Tryte& tryte);
//    void        insert(const std::initializer_list<int>& _initList);
    Tryte findTryte(const int& _key);
    std::string toString() const;
    void        debug();
};

}

#endif // TREE_H
