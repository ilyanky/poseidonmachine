#ifndef VMACHINE_H
#define VMACHINE_H

#include <string>
#include <map>
#include <vector>
#include <machine/Processor.h>
#include <machine/Tree.h>


namespace Poseidon {


class VMachine
{

private:

    typedef void (VMachine::*Instruction)(const std::vector<std::string>& argv);

    Tree m_memory;
    Processor m_processor;

    const std::map<std::string, Instruction> m_instrMap = {
        { std::string("mov"), &VMachine::mov },
        { std::string("sub"), &VMachine::sub }
    };

    const std::map<std::string, RegisterName> m_regMap = {
        { std::string("AX"), AX },  { std::string("BX"), BX },
        { std::string("CX"), CX },  { std::string("DX"), DX },
        { std::string("EX"), EX },  { std::string("FX"), FX },
        { std::string("GX"), GX },  { std::string("HX"), HX },
        { std::string("IX"), IX },
    };



    void mov(const std::vector<std::string>& argv);
    void sub(const std::vector<std::string>& argv);

public:
    VMachine();
    ~VMachine();

    void execute();
    void exec();
};


}

#endif // VMACHINE_H
