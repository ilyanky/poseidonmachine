#include "Processor.h"
#include <string>
#include <iostream>
using namespace Poseidon;

/*
 * Summator
 *---------------------
 */
class Processor::Summator
{
public:
    static RC_Pair sum(const Trit& t1, const Trit& t2, const Trit& c)
    {
        RC_Pair rc = Trit::getRC_Pair(t1, t2);
        rc.carry = Trit::getRemainder(rc.carry, Trit::getCarry(rc.remainder, c));
        rc.remainder = Trit::getRemainder(rc.remainder, c);
        return rc;
    }
};




/*
 * Class methods
 *---------------------
 */
Processor::Processor()
{

}

Processor::~Processor()
{

}


void Processor::move(const RegisterName& name, const Tryte& tryte)
{
    int j = REGISTER_SIZE-1;
    for ( int i = TRYTE_SIZE-1; i > -1 ; --i )
        m_regTable[name][j--] = tryte.at(i);
}


void Processor::summ(const RegisterName& r1, const RegisterName& r2)
{
    Trit c = Z;
    for ( int i = REGISTER_SIZE-1; i > -1 ; --i ) {
        RC_Pair rc = Summator::sum(m_regTable[r1][i], m_regTable[r2][i], c);
        m_regTable[r1][i] = rc.remainder;
        c = rc.carry;
    }
    if ( c != Z )
        throw std::string("Register overflow");
}


std::string Processor::regToString(const RegisterName& name) const
{
    std::string res;
    for ( ushort i = 0; i < REGISTER_SIZE; ++i )
        res += m_regTable.at(name).at(i).toChar();

    return res;
}
