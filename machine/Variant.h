#ifndef VARIANT_H
#define VARIANT_H

#include <lib/Utility.h>
#include <lib/Tryte.h>


namespace Poseidon {


class Variant
{

private:
    Type  m_currentType = Nothing;
    void* m_data;


    void clear(const Type& t);

public:
    Variant();
    Variant(const Tryte& tryte);
    ~Variant();

    Variant& operator=(const Variant& v);

    Tryte getTryte() const;
};


}

#endif // VARIANT_H
