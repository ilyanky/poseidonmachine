#include "Tree.h"
#include <math.h>
#include <string>
using namespace Poseidon;


typedef Node* (Tree::*Rotation)(Node* const) const;

const std::map<BalanceFactor, Rotation> Tree::m_rotationMap = {
        { LeftOverweight, &Tree::rotRight },
        { RightOverweight, &Tree::rotLeft },
        { MidOverweight, &Tree::rotMid },
        { RightTreble, &Tree::rightTreble },
        { LeftTreble, &Tree::leftTreble }
};



Tree::Tree()
{
    m_root = nullptr;
}


Tree::~Tree()
{

}



/*
 *
 * Insert functions
 * ----------------------------------------------
 */
Node* Tree::insert(Node* const _node, Node* const _dataNode) const
{
    if ( _node == nullptr )
        return _dataNode;

    // if equal


    if ( _dataNode->key < _node->key ) {
        if ( _node->left == nullptr || _dataNode->key < _node->left->key )
            _node->left = insert(_node->left, _dataNode);


        else if ( _node->mid == nullptr ) {
            if ( _node->right != nullptr )
                _node->mid = insert(_node->mid, _dataNode);
            else
                _node->left = insert(_node->left, _dataNode);
        }

        else {
            if ( sizeDiff(_node->left, _node->mid, size(_node)) == _node->left )
                _node->left = insert(_node->left, _dataNode);
            else
                _node->mid = insert(_node->mid, _dataNode);
        }

    } else {

        if ( _node->right == nullptr || _dataNode->key > _node->right->key )
            _node->right = insert(_node->right, _dataNode);

        else if ( _node->mid == nullptr ) {
            if ( _node->left != nullptr )
                _node->mid = insert(_node->mid, _dataNode);
            else
                _node->right = insert(_node->right, _dataNode);
        }

        else {
            if ( sizeDiff(_node->right, _node->mid, size(_node)) == _node->right )
                _node->right = insert(_node->right, _dataNode);
            else
                _node->mid = insert(_node->mid, _dataNode);
        }
    }


    fixMeta(_node);
    return balance(_node);
}


void Tree::insert(const int& _key, const Variant& _value)
{
    /*
     *
     * Не забыть про разрешение коллизий!
     * + эту функцию можно оптимизировать, кажется,
     * + если убрать find и изменить insert
     *
     */


    Node* newNode = new Node(_key, _value);
    Node* found = find(m_root, newNode);

    if ( found == nullptr )
        m_root = insert(m_root, newNode);
    else {

        /*
         * later ...
         */

        found->value = _value;

        delete newNode;
    }
}
//-----------------------------------------------



/*
 *
 * Help functions
 * ----------------------------------------------
 */
int Tree::size(const Node* const node) const
{
    return node == nullptr ? 0 : node->size;
}


int Tree::height(const Node* const node) const
{
    return node == nullptr ? 0 : node->height;
}


void Tree::fixHeight(Node* const node) const
{
    int left = height(node->left);
    int mid = height(node->mid);
    int right = height(node->right);

    if ( left > mid ) {
        if ( left > right )
            node->height = left + 1;
        else
            node->height = right + 1;
    } else {
        if ( mid > right )
            node->height = mid + 1;
        else
            node->height = right + 1;
    }
}


void Tree::fixSize(Node* const node) const
{
    if ( node == nullptr )
        return;

    node->size = size(node->left) +
            size(node->mid) +
            size(node->right) + 1;
}


void Tree::fixMeta(Node* const node) const
{
    fixHeight(node);
    fixSize(node);
}


const Node* Tree::sizeDiff(const Node* const leftright, const Node* const mid,
                     const int& pSize) const
{
    int leftrightSize = pSize / 3;  // expected left(or right) size
    int midSize = pSize - leftrightSize * 2; // expected mid size

    std::cout << "---------" << std::endl;
    std::cout << size(leftright) << " " << size(mid) << std::endl;

    if ( abs(size(mid) - midSize) > abs(size(leftright) - leftrightSize) )
        return mid;
    else {
        std::cout << "----------" << std::endl;
        return leftright;
    }
}


int Tree::diff(const Node* const one, const Node* const two) const
{
    if ( one == nullptr )
        return -1;

    if ( two == nullptr )
        return -1;

    return abs(one->size - two->size);
}


void Tree::clear(Node* const node) const
{
    if ( node == nullptr )
        return;

    node->height = 1;
    node->size = 1;
    node->left = nullptr;
    node->mid = nullptr;
    node->right = nullptr;
}


void Tree::_toString(const Node* const _node, std::string* const str) const
{
    if ( _node == nullptr ) {
        str->operator+=(" () ");
        return;
    } else {
        str->operator+=(std::to_string(_node->key));
        str->operator+=("( ");
        str->operator+=("");
    }

    _toString(_node->left, str);
    _toString(_node->mid, str);
    _toString(_node->right, str);

    str->operator+=(" ) ");
}
//-----------------------------------------------------



/*
 *
 * Rotate functions
 * ------------------------------------------------
 */
Node* Tree::leftTreble(Node* const node) const
{
    std::cout << "leftTreble function" << std::endl;

    if ( height(node->right->right) > height(node->right->left) ) {
        node->mid = node->right;
        node->right = node->mid->right;
        node->mid->right = nullptr;
        fixMeta(node->mid);
        fixMeta(node);

        return node;

    } else {
        Node* temp = node->right->left;
        node->right->left = nullptr;
        temp->right = node->right;
        fixMeta(temp->right);

        temp->left = node->left;

        node->left = nullptr;
        node->right = nullptr;
        fixMeta(node);
        temp->mid = node;
        fixMeta(temp);

        return temp;
    }
}


Node* Tree::rightTreble(Node* const node) const
{
    std::cout << "rightTreble function" << std::endl;

    if ( height(node->left->left) > height(node->left->right) ) {
        node->mid = node->left;
        node->left = node->mid->left;
        node->mid->left = nullptr;
        fixMeta(node->mid);
        fixMeta(node);

        return node;

    } else {
        Node* temp = node->left->right;
        node->left->right = nullptr;
        temp->left = node->left;
        fixMeta(temp->left);

        temp->right = node->right;

        node->right = nullptr;
        node->left = nullptr;
        fixMeta(node);
        temp->mid = node;
        fixMeta(temp);

        return temp;
    }
}


Node* Tree::rotLeft(Node* const node) const
{
    std::cout << "rotLeft function" << std::endl;

    Node* temp = node->right;
    temp->mid = insertions(temp->mid, temp->left);

    temp->left = node->left;
    temp->left = insertions(temp->left, node->mid);
    clear(node);
    temp->left = insert(temp->left, node);

    fixMeta(temp);
    return temp;
}


Node* Tree::rotRight(Node* const node) const
{
    std::cout << "rotRight function" << std::endl;

    Node* temp = node->left;
    temp->mid = insertions(temp->mid, temp->right);

    temp->right = node->right;
    temp->right = insertions(temp->right, node->mid);
    clear(node);
    temp->right = insert(temp->right, node);

    fixMeta(temp);
    return temp;
}


Node* Tree::rotMid(Node* const node) const
{
    std::cout << "rotMid function" << std::endl;

    Node* temp = node->mid;
    temp = insertions(temp, node->left);
    temp = insertions(temp, node->right);
    clear(node);
    temp = insert(temp, node);

    return temp;
}
//--------------------------------------------------------



/*
 *
 * Balance function
 * ------------------------------------------------
 */
Node* Tree::balance(Node* const node) const
{
    std::cout << "balance function" << std::endl;

    int left = height(node->left);
    int mid = height(node->mid);
    int right = height(node->right);

    if ( left != 0 && right != 0 && mid == 0 ) {
        if ( right - left >= 1 )
            return (this->*m_rotationMap.at(LeftTreble))(node);
        else if ( left - right >= 1 )
            return (this->*m_rotationMap.at(RightTreble))(node);
        else
            return node;
    }

    if ( left - right >= 2 )
        return (this->*m_rotationMap.at(LeftOverweight))(node);
    else if ( right - left >= 2 )
        return (this->*m_rotationMap.at(RightOverweight))(node);
    else if ( mid - right >= 2 )
        return (this->*m_rotationMap.at(MidOverweight))(node);
    else if ( mid - left >= 2 )
        return (this->*m_rotationMap.at(MidOverweight))(node);
    else
        return node;
}
//--------------------------------------------------------



Node* Tree::find(Node* const _node, const Node* const dataNode) const
{
    if ( _node == nullptr )
        return nullptr;

    if ( _node->key == dataNode->key )
        return _node;



    if ( dataNode->key < _node->key ) {
        if ( _node->left != nullptr && dataNode->key < _node->left->key )
            return find(_node->left, dataNode);

        else {
            Node* temp = find(_node->left, dataNode);
            if ( temp == nullptr )
                return find(_node->mid, dataNode);
            else
                return temp;
        }

    } else {

        if ( _node->right != nullptr && dataNode->key > _node->right->key )
            return find(_node->right, dataNode);

        else {
            Node* temp = find(_node->mid, dataNode);
            if ( temp != nullptr )
                return temp;
            else
                return find(_node->right, dataNode);
        }
    }
}



Node* Tree::insertions(Node* _node, Node* const _dataNode) const
{
    if ( _dataNode == nullptr )
        return _node;

    _node = insertions(_node, _dataNode->left);
    _node = insertions(_node, _dataNode->mid);
    _node = insertions(_node, _dataNode->right);

    clear(_dataNode);
    return insert(_node, _dataNode);
}




/*
 *
 * Public interface functions
 * ------------------------------------------------
 */
void Tree::insertTryte(const int& key, const Tryte& tryte)
{
    insert(key, Variant(tryte));
}


Tryte Tree::findTryte(const int& _key)
{
    Node newNode(_key);
    Node* found = find(m_root, &newNode);
    if ( found == nullptr )
        throw std::string("there is no such element");
    else
        return found->value.getTryte();
}


//void Tree::insert(const std::initializer_list<int>& _initList)
//{
//    for ( int i : _initList )
//        insert(i);
//}


std::string Tree::toString() const
{
    std::string str;
    _toString(m_root, &str);
    return str;
}
//-------------------------------------------------






void Tree::debug()
{
    for ( int i = 1; i <= 16; ++i ) {
        this->insert(i, Variant(Tryte({T})));
        std::cout << this->height(this->m_root) << " -- height" << std::endl;
        std::cout << this->size(this->m_root) << " -- size" << std::endl;
    }

    this->insert(17, Variant(Tryte({T})));
    std::cout << this->height(this->m_root) << " -- height" << std::endl;
    std::cout << this->size(this->m_root) << " -- size" << std::endl;


    Node node(15, Variant(Tryte({T})));
    Node* res = find(m_root, &node);
    std::cout << res << "   " << res->key << std::endl;


//    m_root = this->rotLeft(m_root);
//    std::cout << this->height(this->m_root) << " -- height" << std::endl;
//    std::cout << this->size(this->m_root) << " -- size" << std::endl;



    std::cout << this->toString() << std::endl;
}
