#ifndef PROCESSOR_H
#define PROCESSOR_H


#define REGISTER_NUM   9
#define REGISTER_SIZE  27


#include <lib/Trit.h>
#include <string>
#include <lib/Tryte.h>


namespace Poseidon {

class Processor
{

private:
    /*
     * Register Table
     *---------------------
     */
    struct RegisterTable
    {
    public:
        struct Register
        {
        private:
            Trit reg[REGISTER_SIZE];

        public:
            Trit& operator[](const uint& i)
            {
                if ( i >= REGISTER_SIZE )
                    throw std::string("Out of Register bounds");

                return reg[i];
            }

            Trit at(const uint& i) const
            {
                if ( i >= REGISTER_SIZE )
                    throw std::string("Out of Register bounds");

                return reg[i];
            }
        };

        Register table[REGISTER_NUM];

    public:
        Register& operator[](const RegisterName& name)
        {
            return table[name];
        }

        Register at(const RegisterName& name) const
        {
            return table[name];
        }

    };

    class  Summator;


    RegisterTable m_regTable;


public:
    Processor();
    ~Processor();


    void move(const RegisterName& name, const Tryte& tryte);
    void summ(const RegisterName& r1, const RegisterName& r2);


    std::string regToString(const RegisterName& name) const;
};


}

#endif // PROCESSOR_H
