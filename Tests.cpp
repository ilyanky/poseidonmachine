#include "Tests.h"
#include <lib/Tryte.h>
#include <iostream>
#include <fstream>
#include <machine/VMachine.h>
using namespace Poseidon;

void logicTest()
{
    Trit t1 = T;
    Trit t2 = Z;
    Trit t3 = U;

    Trit _t1 = ~t1;
    Trit _t2 = ~t2;
    Trit _t3 = ~t3;

    std::cout << t1.toChar() << "\t" << _t1.toChar() << "\n";
    std::cout << t2.toChar() << "\t" << _t2.toChar() << "\n";
    std::cout << t3.toChar() << "\t" << _t3.toChar() << "\n";
}


void otherTests()
{
    Tryte tryte = { T, T, T, U, U, U, Z, Z, Z };
    std::cout << tryte.toString() << std::endl;
    std::cout << tryte.allToString() << std::endl;
}


void sumTest()
{
    Tryte tr1 = { U, Z, Z, Z };
    Tryte tr2 = { T };

    tr1 += tr2;
    std::cout << tr1.toString() << std::endl;
}


void negativeTest()
{
    Tryte tryte4 = { T, T };
    Tryte tryte5 = { Z, T };
    Tryte tryte6;
    std::cout << (-tryte4).toString() << std::endl;
    std::cout << (-tryte5).toString() << std::endl;
    std::cout << (-tryte6).toString() << std::endl;
}


void conversionTest()
{
    std::cout << Tryte::fromDecimal(0).toString() << std::endl;
    std::cout << Tryte::fromDecimal(10).toString() << std::endl;
    std::cout << Tryte::fromDecimal(25).toString() << std::endl;
    std::cout << Tryte::fromDecimal(34).toString() << std::endl;
    std::cout << Tryte::fromDecimal(48).toString() << std::endl;
    std::cout << Tryte::fromDecimal(361).toString() << std::endl;
    std::cout << Tryte::fromDecimal(405).toString() << std::endl;
    std::cout << Tryte::fromDecimal(1900).toString() << std::endl;

    std::cout << Tryte::fromDecimal(0).toString() << std::endl;
    std::cout << Tryte::fromDecimal(-10).toString() << std::endl;
    std::cout << Tryte::fromDecimal(-25).toString() << std::endl;
    std::cout << Tryte::fromDecimal(-34).toString() << std::endl;
    std::cout << Tryte::fromDecimal(-48).toString() << std::endl;
    std::cout << Tryte::fromDecimal(-361).toString() << std::endl;
    std::cout << Tryte::fromDecimal(-405).toString() << std::endl;
    std::cout << Tryte::fromDecimal(-1900).toString() << std::endl;

    std::cout << Tryte::fromDecimal(9842).toString() << std::endl;
}


void multiTest()
{
    Tryte tr1 = { U, U, U };
    Trit t1 = Z;

    Tryte tr3 = { T, Z, T };
    Tryte tr4 = { T, Z, T };

    std::cout << (tr3*tr4).toString() << std::endl;
}


void shiftTest()
{
    Tryte tr1 = { Z, U, T, U, T, Z, Z, T, T };
    std::cout << (tr1 << 1).toString() << std::endl;
}


void compTest()
{
    Tryte tr1 = { U, T, U, Z };
    Tryte tr2 = { U, Z, T };

    Tryte tr3 = { T, T, U };
    Tryte tr4 = { U, T, Z };

    std::cout << tr3.allToString() << std::endl;
    std::cout << tr4.allToString() << std::endl;
    std::cout << tr3.abs().allToString() << std::endl;
    std::cout << tr4.abs().allToString() << std::endl;

    if ( tr3.abs() <= tr4.abs() )
        std::cout << "true" << std::endl;
    else
        std::cout << "false" << std::endl;
}


void divTest()
{
    Tryte tr1 = { U, T, Z, T, U, Z };
    Tryte tr2 = { U, T, Z };

    Tryte tr3 = { U, T, Z, T, T, U };
    Tryte tr4 = { U, T, Z };

    Tryte tr5 = { U, U, U };
    Tryte tr6 = { U, U, U };

    Tryte tr7 = { U, T, T, Z, Z };
    Tryte tr8 = { U, U, T, Z };

    Tryte tr9 = { U, T, Z, T, Z, Z, T, T, T };
    Tryte tr10 = { U, T };

    Tryte tr11 = { T, U, Z, U, Z, Z, U, U, U };
    Tryte tr12 = { U, T };

    Tryte tr13 = { T, U, Z, U, Z, Z, U, U, U };
    Tryte tr14 = { T, U };

    std::cout << (tr1 / tr2).toString() << std::endl;
    std::cout << (tr3 / tr4).toString() << std::endl;
    std::cout << (tr5 / tr6).toString() << std::endl;
    std::cout << (tr7 / tr8).toString() << std::endl;
    std::cout << (tr9 / tr10).toString() << std::endl;
    std::cout << (tr11 / tr12).toString() << std::endl;
    std::cout << (tr13 / tr14).toString() << std::endl;
}


void fromStringTest()
{
    try {
        Tryte tr = Tryte::fromString("1T0");
        std::cout << tr.toString() << std::endl;
    } catch ( std::string s ) {
        std::cout << s << std::endl;
    }
}


void VM_Test()
{
    std::ofstream fout("instructions");
    fout << "mov cx T10\n";
    fout << "mov sx T110\n";
    fout << "sub cx sx\n";
    fout.close();

    try {
        VMachine vm;
        vm.execute();
    } catch ( std::string& e ) {
        std::cout << e << std::endl;
    }
}
