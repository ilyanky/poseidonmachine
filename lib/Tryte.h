#ifndef TRYTE_H
#define TRYTE_H

#include <lib/Trit.h>
#include <initializer_list>
#include <string>

#define TRYTE_SIZE     9
#define TRYTE_MAX_VAL  9841


namespace Poseidon {


class Tryte
{

private:
    Trit   m_dataArr[TRYTE_SIZE];

    Trit&  operator[](const uint& _pos);

    Tryte  getFirst(const ushort& _n = 1) const;
    Tryte  subdiv(const Tryte& _divisor, Tryte* const _remainder) const;

public:
    Tryte();
    Tryte(const std::initializer_list<Trit>& _initList);
    ~Tryte();


    Trit         at(const uint& _pos) const;

    Tryte        operator+(const Tryte& _addend) const;
    Tryte        operator-() const;
    Tryte        operator<<(const ushort& _num) const;
    Tryte        operator*(const Trit& _trit) const;
    Tryte        operator*(const Tryte& _multiplier) const;
    Tryte        operator-(const Tryte& _subtrahend) const;
    Tryte        operator/(const Tryte& _divisor) const;

    bool         operator<(const Tryte& _tryte) const;
    bool         operator<=(const Tryte& _tryte) const;
    bool         operator>=(const Tryte& _tryte) const;

    Tryte        operator=(const Tryte& _tryte);
    Tryte        operator+=(const Tryte& _addend);
    Tryte        operator-=(const Tryte& _subtrahend);

    std::string  toString() const;
    std::string  allToString() const;
    ushort       firstSgnfTrit() const;
    ushort       firstSgnfTrit(const Tryte& _tryte) const;
    ushort       lastSgnfTrit() const;
    ushort       sgnfSize() const;
    bool         isZero() const;
    bool         isNegative() const;
    Tryte        abs() const;

    static Tryte fromDecimal(const short& _decimal);
    static Tryte negative(const Tryte& _tryte);
    static Tryte fromString(const std::string& str);
    static Tryte sub(const Tryte& one, const Tryte& two);
};


}

#endif // TRYTE_H
