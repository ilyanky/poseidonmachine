#ifndef TRIT_H
#define TRIT_H

#include <lib/Utility.h>

namespace Poseidon {


class Trit
{

private:
    static const RC_Pair   rc_table[3][3];
    static const Trinity   multi_table[3][3];

    static ushort getIndex(const Trit& _trit);

    Trinity m_tr;

public:
    Trit();
    Trit(const Trinity& _tr);
    Trit(const short& _decimal);
    ~Trit();


    static RC_Pair  getRC_Pair(const Trit& _first, const Trit& _second);
    static Trinity  getRemainder(const Trit& _first, const Trit& _second);
    static Trinity  getCarry(const Trit& _first, const Trit& _second);

    static Trinity  getProd(const Trit& _first, const Trit& _second);


    Trinity get() const;

    Trit    operator~() const;
    Trit    operator&(const Trit& _trit) const;
    Trit    operator^(const Trit& _trit) const;
    bool    operator==(const Trit& _trit) const;
    bool    operator!=(const Trit& _trit) const;
    bool    operator<(const Trit& _trit) const;
    bool    operator<=(const Trit& _trit) const;

    char    toChar() const;
};


}

#endif // TRIT_H
