#ifndef UTILITY
#define UTILITY

#include <utility>


typedef unsigned short ushort;
typedef unsigned int uint;

namespace Poseidon {



/*
 * Enum for Variant types
 *-------------------------
 */
enum Type {
    Tryte_t,
    Trit_t,
    Nothing
};




/*
 * Enum for ternary values
 *-------------------------
 */
enum Trinity {
    T, // -1 or T
    Z, //  0
    U  //  1
};



/*
 * Enum for register names
 *---------------------------
 */
enum RegisterName {
    AX, BX,
    CX, DX,
    EX, FX,
    GX, HX,
    IX
};



/*
 * A pair for remainder and carry
 *----------
 */
struct RC_Pair
{
    Trinity remainder;
    Trinity carry;


    RC_Pair(const Trinity& _remainder, const Trinity& _carry)
    {
        remainder = _remainder;
        carry = _carry;
    }

    Trinity getRemainder()
    {
        return remainder;
    }

    Trinity getCarry()
    {
        return carry;
    }
};


}

#endif // UTILITY

