#include "Trit.h"
#include <iostream>
using namespace Poseidon;


const RC_Pair Trit::rc_table[3][3] = {
       { RC_Pair(U, T), RC_Pair(T, Z), RC_Pair(Z, Z) },
       { RC_Pair(T, Z), RC_Pair(Z, Z), RC_Pair(U, Z) },
       { RC_Pair(Z, Z), RC_Pair(U, Z), RC_Pair(T, U) }
};

const Trinity Trit::multi_table[3][3] = {
    { U, Z, T },
    { Z, Z, Z },
    { T, Z, U }
};





Trit::Trit()
{
    m_tr = Z;
}


Trit::Trit(const Trinity& _tr)
{
    m_tr = _tr;
}


Trit::Trit(const short& _decimal)
{
    if ( _decimal < 0 )
        m_tr = T;

    else if ( _decimal > 0 )
        m_tr = U;

    else
        m_tr = Z;
}


Trit::~Trit()
{

}





ushort Trit::getIndex(const Trit& _trit)
{
    if ( _trit.get() == T )
        return 0;
    else if ( _trit.get() == Z )
        return 1;
    else
        return 2;
}


RC_Pair Trit::getRC_Pair(const Trit& _first, const Trit& _second)
{
    int i = getIndex(_first);
    int j = getIndex(_second);
    return rc_table[i][j];
}


Trinity Trit::getRemainder(const Trit& _first, const Trit& _second)
{
    return getRC_Pair(_first, _second).getRemainder();
}


Trinity Trit::getCarry(const Trit& _first, const Trit& _second)
{
    return getRC_Pair(_first, _second).getCarry();
}


Trinity Trit::getProd(const Trit& _first, const Trit& _second)
{
    int i = getIndex(_first);
    int j = getIndex(_second);
    return multi_table[i][j];
}




Trinity Trit::get() const
{
    return m_tr;
}


Trit Trit::operator~() const
{
    if ( m_tr == T )
        return Trit(U);

    else if ( m_tr == U )
        return Trit(T);

    else
        return Trit(Z);
}


Trit Trit::operator&(const Trit& _trit) const
{
    /*
     *  ... later
     *
     **/

    return Z;
}


Trit Trit::operator^(const Trit& _trit) const
{
    /*
     *  ... later
     *
     **/

    return Z;
}


bool Trit::operator==(const Trit& _trit) const
{
    return this->m_tr == _trit.m_tr;
}


bool Trit::operator!=(const Trit& _trit) const
{
    return !this->operator==(_trit);
}


bool Trit::operator<(const Trit& _trit) const
{
    if ( this->m_tr == T && _trit.m_tr != T )
        return true;

    if ( this->m_tr == Z && _trit.m_tr == U )
        return true;

    return false;
}


bool Trit::operator<=(const Trit& _trit) const
{
    if ( this->operator<(_trit) || this->operator==(_trit) )
        return true;

    return false;
}






char Trit::toChar() const
{
    if ( m_tr == T )
        return 'T';

    else if ( m_tr == Z )
        return '0';

    else
        return '1';
}

