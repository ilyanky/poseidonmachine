#include "Tryte.h"
#include <iostream>
#include <stack>
using namespace Poseidon;


Tryte::Tryte()
{
    for ( Trit i : m_dataArr )
        i = Z;
}


Tryte::Tryte(const std::initializer_list<Trit>& _initList)
{
    Tryte();

    std::initializer_list<Trit>::const_iterator ii = _initList.end();
    --ii;
    for ( short i = TRYTE_SIZE-1; i >= 0; --i, --ii ) {
        m_dataArr[i] = *ii;

        if ( ii == _initList.begin() )
            break;
    }
}


Tryte::~Tryte()
{

}




//-------access operations----------------
Trit& Tryte::operator[](const uint& _pos)
{
    if ( _pos >= TRYTE_SIZE )
        throw std::string("Out of bounds");

    return this->m_dataArr[_pos];
}


Trit Tryte::at(const uint &_pos) const
{
    if ( _pos >= TRYTE_SIZE )
        throw std::string("Out of bounds");

    return this->m_dataArr[_pos];
}
//-----------------------------------------




//----------arithmetic operations----------
Tryte Tryte::operator+(const Tryte& _addend) const
{
    Tryte res;

    if ( _addend.isZero() )
        return *this;

    Trit buff = Z;
    short count = this->firstSgnfTrit(_addend);
    for ( short i = TRYTE_SIZE-1; i >= count; --i ) {
        if ( buff != Z ) {
            RC_Pair temp = Trit::getRC_Pair(buff, this->at(i));
            res[i] = Trit::getRemainder(temp.getRemainder(), _addend.at(i));

            buff = Trit::getCarry(temp.getRemainder(), _addend.at(i));
            buff = Trit::getRemainder(temp.getCarry(), buff);
        } else {
            res[i] = Trit::getRemainder(this->at(i), _addend.at(i));
            buff = Trit::getCarry(this->at(i), _addend.at(i));
        }
    }
    if ( buff != Z ) {
        if ( count == 0 ) {
            res = Tryte();
            throw std::string("Tryte Overflow");
        }
        else
            res[count-1] = buff;
    }


    return res;
}


Tryte Tryte::operator-() const
{
    return negative(*this);
}


Tryte Tryte::operator<<(const ushort& _num) const
{
    Tryte res;

    short num = _num;
    if ( num >= TRYTE_SIZE )
        num = num % TRYTE_SIZE;

    for ( short i = TRYTE_SIZE-num-1; i >= 0; --i )
        res[i] = this->at(i+num);
    for ( ushort i = TRYTE_SIZE-1; i >= TRYTE_SIZE-num; --i )
        res[i] = this->at(i-TRYTE_SIZE+num);

    return res;
}


Tryte Tryte::operator*(const Trit& _trit) const
{
    if ( _trit == T )
        return this->operator-();
    else if ( _trit == Z )
        return Tryte();
    else
        return *this;
}


Tryte Tryte::operator*(const Tryte& _multiplier) const
{
    Tryte res;

    short count = this->firstSgnfTrit(_multiplier);
    for ( short i = TRYTE_SIZE-1; i >= count; --i ) {
        if ( _multiplier.at(i) == Z )
            continue;
        Tryte temp = (*this * _multiplier.at(i)) << TRYTE_SIZE - (i + 1);
        res += temp;
    }

    return res;
}


Tryte Tryte::operator-(const Tryte& _subtrahend) const
{
    return this->operator+(-_subtrahend);
}


Tryte Tryte::operator/(const Tryte& _divisor) const
{
    ushort dividendSize = this->sgnfSize();
    ushort divisorSize = _divisor.sgnfSize();

    if ( dividendSize < divisorSize )
        return Tryte();

    Tryte res;

    Tryte dividend = *this;
    Tryte divisor = _divisor;
    if ( this->isNegative() )
        dividend = negative(dividend);
    if ( _divisor.isNegative() )
        divisor = negative(_divisor);

    if ( dividendSize == divisorSize ) {
        Tryte remainder = divisor;
        while ( divisor <= remainder ) {
            res = res + Tryte({ U });
            remainder -= divisor;
        }
        return res;
    }

    Tryte remainder = dividend.getFirst(divisorSize);
    std::stack<Trit> stack;
    for ( short i = TRYTE_SIZE-1; i >= dividend.firstSgnfTrit()+divisorSize; --i )
        stack.push(dividend.at(i));

    while ( !stack.empty() ) {
        if ( remainder < divisor ) {
            remainder = remainder << 1;
            remainder[TRYTE_SIZE-1] = stack.top();
            stack.pop();
        }
        res = res << 1;
        res += subdiv(divisor, &remainder);
    }
    if ( remainder.isNegative() ) {
        remainder -= (divisor * T);
        res += Tryte({ T });
    }

    return res;
}
//-----------------------------------------



//--------non const operations-------------
Tryte Tryte::operator=(const Tryte& _tryte)
{
    for ( short i = 0; i < TRYTE_SIZE; ++i )
        this->operator[](i) = _tryte.at(i);

    return *this;
}


Tryte Tryte::operator+=(const Tryte& _addend)
{
    *this = this->operator+(_addend);
    return *this;
}


Tryte Tryte::operator-=(const Tryte& _subtrahend)
{
    *this = this->operator-(_subtrahend);
    return *this;
}
//-----------------------------------------



//-------comparison operations-------------
bool Tryte::operator<(const Tryte& _tryte) const
{
    short count = this->firstSgnfTrit(_tryte);
    for ( short i = count; i < TRYTE_SIZE; ++i )
        if ( this->at(i) != _tryte.at(i) ) {
            if ( this->at(i) < _tryte.at(i) )
                return true;
            else
                return false;
        }

    return false;
}


bool Tryte::operator<=(const Tryte& _tryte) const
{
    short count = this->firstSgnfTrit(_tryte);
    for ( ushort i = count; i < TRYTE_SIZE; ++i )
        if ( this->at(i) < _tryte.at(i) )
            return true;
        else if ( this->at(i) == _tryte.at(i) )
            continue;
        else
            return false;

    return true;
}


bool Tryte::operator>=(const Tryte& _tryte) const
{
    return this->operator<=(_tryte) ? false : true;
}
//-----------------------------------------



//-------help functions--------------------
ushort Tryte::firstSgnfTrit() const
{
    for ( ushort i = 0; i < TRYTE_SIZE; ++i ) {
        if ( m_dataArr[i] != Z )
            return i;
    }

    return TRYTE_SIZE-1;
}


ushort Tryte::firstSgnfTrit(const Tryte& _tryte) const
{
    ushort first = 0;
    if ( _tryte.firstSgnfTrit() <= this->firstSgnfTrit() )
        first = _tryte.firstSgnfTrit();
    else
        first = this->firstSgnfTrit();

    return first;
}


ushort Tryte::lastSgnfTrit() const
{
    return TRYTE_SIZE - (firstSgnfTrit() + 1);
}


ushort Tryte::sgnfSize() const
{
    return lastSgnfTrit() + 1;
}


bool Tryte::isZero() const
{
    ushort fst = firstSgnfTrit();
    if ( fst == TRYTE_SIZE-1 && this->at(fst) == Z )
        return true;

    return false;
}


bool Tryte::isNegative() const
{
    if ( this->at(this->firstSgnfTrit()) == T )
        return true;
    else
        return false;
}


Tryte Tryte::abs() const
{
    if ( this->isNegative() )
        return negative(*this);
    else
        return *this;
}


std::string Tryte::allToString() const
{
    std::string res;
    for ( ushort i = 0; i < TRYTE_SIZE; ++i )
        res += m_dataArr[i].toChar();

    return res;
}


std::string Tryte::toString() const
{
    std::string res;
    for ( ushort i = firstSgnfTrit(); i < TRYTE_SIZE; ++i )
        res += m_dataArr[i].toChar();

    return res;
}
//-----------------------------------------



//-------private help functions------------
Tryte Tryte::getFirst(const ushort& _n) const
{
    if ( _n > TRYTE_SIZE )
        throw std::string("Too big _n. From getFirst");

    Tryte res;

    ushort n = lastSgnfTrit() + 1;
    if ( n > _n )
        n = _n;

    for ( ushort i = firstSgnfTrit(), j = 0; j < n; ++i, ++j )
    {
        res = res << 1;
        res[TRYTE_SIZE-1] = this->at(i);
    }

    return res;
}


Tryte Tryte::subdiv(const Tryte& _divisor, Tryte* const _remainder) const
{
    Tryte res;
    Tryte adivisor = _divisor.abs();
    while ( adivisor <= _remainder->abs() ) {
        if ( _remainder->isNegative() ) {
            *_remainder -= (_divisor * T);
            res += Tryte({ T });
        } else {
            *_remainder -= _divisor;
            res += Tryte({ U });
        }
    }

    return res;
}
//-----------------------------------------



//-------static functions------------------
Tryte Tryte::fromDecimal(const short& _decimal)
{
    if ( _decimal > TRYTE_MAX_VAL )
        throw std::string("Too big decimal number. fromDecimal called\n");

    Tryte res;
    short temp = _decimal;

    if ( _decimal < 0 )
        temp = -_decimal;

    uint i = TRYTE_SIZE - 1;
    while ( temp >= 3 ) {
        if ( temp % 3 == 2 ) {
            temp = temp / 3 + 1;
            res[i--] = T;
        } else {
            res[i--] = temp % 3;
            temp = temp / 3;
        }
    }
    if ( temp != 2 )
        res[i--] = temp;
    else {
        res[i--] = T;
        res[i--] = U;
    }


    if ( _decimal < 0 )
        return negative(res);
    else
        return res;
}


Tryte Tryte::negative(const Tryte& _tryte)
{
    Tryte res;

    for ( ushort i = 0; i < TRYTE_SIZE; ++i )
        res[i] = ~_tryte.at(i);

    return res;
}


Tryte Tryte::fromString(const std::string& str)
{
    Tryte res;

    if ( str.size() > TRYTE_SIZE )
        throw std::string("too big str");

    int pos = TRYTE_SIZE-1;
    for ( int i = str.size()-1; i != -1; --i ) {
        if ( str.at(i) == 'T' )
            res[pos--] = T;
        else if ( str.at(i) == '1' )
            res[pos--] = U;
        else if ( str.at(i) == '0' )
            res[pos--] = Z;
        else
            throw std::string("wrong string");
    }

    return res;
}


Tryte Tryte::sub(const Tryte& one, const Tryte& two)
{
    return one.operator+(-two);
}
//-----------------------------------------
