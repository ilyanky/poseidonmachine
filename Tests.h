#ifndef TESTS_H
#define TESTS_H

void shiftTest();
void multiTest();
void conversionTest();
void negativeTest();
void sumTest();
void otherTests();
void logicTest();
void compTest();
void divTest();
void fromStringTest();
void VM_Test();

#endif // TESTS_H
