#include <iostream>
#include <lib/Tryte.h>
#include <Tests.h>
#include <fstream>
#include <machine/VMachine.h>

using namespace Poseidon;

int main()
{
    std::ofstream fout("instructions");
    fout << "mov AX T10\n";
    fout << "mov BX T110\n";
    fout << "sum AX BX\n";
    fout.close();

    try {
        VMachine vm;
        vm.exec();
    } catch ( std::string& e ) {
        std::cout << e << std::endl;
    }

    return 0;
}
